#pragma once

#include <iostream>
#include <torch/script.h>

namespace myHcn {

class Hcn
{
public:
    std::vector<torch::jit::IValue> inputs;
    c10::IValue output;

    Hcn(const std::string & modelFile);
    ~Hcn() = default;

    torch::Tensor getHc();
    torch::Tensor run(float tfValue);
    int shotOverCallback();

private:
    torch::jit::script::Module model;
    torch::Tensor hc;                       // 额外传入model的参数
    bool setHc();
    bool isRunning;
};

}   // namespace myHcn
