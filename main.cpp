#include <iostream>
#include <chrono>
#include <torch/script.h>
#include "hcn.h"

using namespace std;
using namespace myHcn;

int main(int argc, const char *argv[]) 
{
    string modelFile("../hcn2.pt");
    Hcn pt = Hcn(modelFile);

    // 接收数据，运行部分
    torch::Tensor hcnValue = pt.run(9);
    cout << "==================" << endl;
    cout << hcnValue << endl;

    hcnValue = pt.run(10);
    cout << "==================" << endl;
    cout << hcnValue << endl;

    // 一炮停止标志位
    pt.shotOverCallback();

    float itValue = 0.1;
    auto start = chrono::high_resolution_clock::now();
    for (int i=0; i<10000; ++i) {
        itValue += 0.001;
        hcnValue = pt.run(itValue);
    }
    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> elapsed = end - start;
    cout << "Elapsed time: " << elapsed.count() << " seconds" << std::endl;

    // 一炮停止标志位
    pt.shotOverCallback();

    return 0;
}