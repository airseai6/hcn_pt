#include <torch/script.h> // One-stop header.

#include <iostream>
#include <tuple>
#include <memory>

int main(int argc, const char* argv[]) {
    // if (argc != 3) {
    //     std::cerr << "usage: example-app <path-to-exported-script-module> <itf-value>\n";
    //     return -1;
    // }


    torch::jit::script::Module module;
    torch::Tensor tensor1;
    torch::Tensor tensor2;
    try {
        // Deserialize the ScriptModule from a file using torch::jit::load().
        // module = torch::jit::load(argv[1]);
        // float tf_value = std::stof(argv[2]);
        
        module = torch::jit::load(std::string("../hcn.pt"));
        float tf_value = std::stof(std::string("9"));
        
        tensor1 = torch::tensor({tf_value});
        tensor2 = torch::zeros({4,1,1,96});
    }
    catch (const c10::Error& e) {
        std::cerr << "error loading the model\n";
        std::cerr << e.what();
        return -1;
    }

    // 运行模型
    std::vector<torch::jit::IValue> inputs;
    // inputs.push_back(input_value);
    inputs.push_back(tensor1);
    inputs.push_back(tensor2);
    // torch::Tensor output = module.forward(inputs).toTensor();
    c10::IValue value = module.forward(inputs);
    torch::Tensor output1 = value.toTuple()->elements()[0].toTensor();
    torch::Tensor output2 = value.toTuple()->elements()[1].toTensor();
    // 打印输出结果
    std::cout << output1 << std::endl;
    std::cout << output2 << std::endl;

    return 0;
}
